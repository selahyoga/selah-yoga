Selah Yoga is a safe, inspirational place to relax and reset from the demands of life. All Selah classes begin and end with a moment of mindful meditation and an inspirational message. Selah instructors are highly trained, certified and focused on providing you with a wonderful yoga experience.

Address: 526 E 2nd St, Pass Christian, MS 39571, USA

Phone: 228-219-2788
